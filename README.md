# Transfer Learning Introduction for 4 April Snacks-N-Hacks

https://www.meetup.com/Snacks-hacks-Amsterdam/events/259186880/

---

![Transfer](https://i.imgflip.com/2xof9l.jpg)

---

## Running Instructions

### 1 - Install Docker 

Install DockerCommunity Edition (CE) 

https://docs.docker.com/install/

### 2 - Pull the Official Docker Tensorflow Image `:latest-py3-jupyter`

```
docker pull tensorflow/tensorflow:latest-py3-jupyter
```

### 3 - Clone/Download this Git Repo

```
git clone https://gitlab.com/whendrik/intro_transfer_learning.git
```

### 4 - run `docker.sh` or manually start the tensorflow image

Within the freshly pulled git repo, do a
```
./docker.sh 
```

or - alternatively - manually start the docker container

```
docker run --rm -it -p 5000:5000 -p 8888:8888 -v "`pwd`":/tf/transferlearning tensorflow/tensorflow:latest-py3-jupyter
```

### 5 - Open the URL shown in the docker logs

e.g.
```
[I 11:11:52.595 NotebookApp] Writing notebook server cookie secret to /root/.local/share/jupyter/runtime/notebook_cookie_secret
jupyter_http_over_ws extension initialized. Listening on /http_over_websocket
[I 11:11:52.821 NotebookApp] Serving notebooks from local directory: /tf
[I 11:11:52.822 NotebookApp] The Jupyter Notebook is running at:
[I 11:11:52.822 NotebookApp] http://(8d351323ffa5 or 127.0.0.1):8888/?token=a0152210292775a12b3d4947d158ae6fc3c1cc8799905c7c
[I 11:11:52.822 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 11:11:52.826 NotebookApp] 
    
    To access the notebook, open this file in a browser:
        file:///root/.local/share/jupyter/runtime/nbserver-9-open.html
    Or copy and paste one of these URLs:
        http://(8d351323ffa5 or 127.0.0.1):8888/?token=a0152210292775a12b3d4947d158ae6fc3c1cc8799905c7c
```

...and within the `transferlearning` folder open `Transfer_Learning_Intro.ipynb` and follow the notebook instructions.

Try to answer all the questions within the notebook.